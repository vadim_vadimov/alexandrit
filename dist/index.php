<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ЖК Александрит в Сочи - Официальный сайт застройщика</title>
	<meta name="description" content="ЖК Александрит в Сочи - квартиры бизнес-класса в центре Сочи. 10 минут от моря. Напрямую от застройщика.">
	<meta name="keywords" content="Александрит, Сочи, Застройщик, Центр, ЖК, Дом, Квартиры">

	<link rel="stylesheet"  href="css/style.css">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">

	<!-- Facebook Pixel Code -->
	<script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '297698297598052');
        fbq('track', 'PageView_aleksandrit');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=297698297598052&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135295071-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135295071-1');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52574287, "init", {
        id:52574287,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52574287" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- pixel VK -->
<script>
    // переменная для хранения экземпляра пикселя
    var pixel;
    // вызовется после загрузки openapi.js
    window.vkAsyncInit = function() {
        pixel = new VK.Pixel('VK-RTRG-346936-2n9zq');
        VK.Retargeting.Init('VK-RTRG-346936-2n9zq');
        VK.Retargeting.Hit();
    }
</script>
<script src="//vk.com/js/api/openapi.js?159" async></script>
<!-- /pixel VK -->

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({id: "3090707", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = "https://top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script><noscript><div>
        <img src="https://top-fwz1.mail.ru/counter?id=3090707;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
    </div></noscript>
<!-- //Rating@Mail.ru counter -->

<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
<script type="text/javascript">
    var _tmr = _tmr || [];
    _tmr.push({
        type: 'itemView'
    });
</script>
<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->

	<div class="main-wrap">
		<div class="intro-block" style="background-image: url(img/intro-bg.jpg);">
			<header class="header">
				<div class="container">
					<div class="header__logo-content">
						<a class="header__logo" href="/">
							<img src="img/logo.svg" alt="logo">
							<span>Жилой комплекс бизнес класса</span>
						</a>
						<div class="header__item">
							<img src="img/intro-house.png" alt="house">
							<div class="header__item-text">
								<strong>Сдача дома</strong>
								<p>IV квартал 2020</p>
							</div>
						</div>
					</div>
					<div class="header__info">
						<a href="tel:89683002099" class="header__tel jsTrackContact">8 968 300 20 99</a>
						<a data-fancybox data-src="#popup-1" href="javascript://" class="btn-main  btn-main--small"><span>Записаться на показ</span></a>
					</div>
				</div>
			</header>

			<section class="intro">
				<div class="container">
					<div class="intro__content">
						<h1 class="intro__title">Жилой комплекс <span class="title-mask"><span class="title-arrows">бизнес-класса</span></span><br>в центре Сочи</h1>
						<a data-fancybox data-src="#popup-2" href="javascript://" class="btn-main  btn-main--col"><span>Получить доступные квартиры<br>и актуальные цены</span></a>
						<div class="intro__list">
							<div class="intro__item" style="background-image: url(img/intro-item-1.png);">
								<strong>панорамные виды</strong>
								<p>на море и горы</p>
								<div class="number-item">
									<span>01</span>
								</div>
							</div>
							<div class="intro__item  intro__item--white" style="background-image: url(img/intro-item-2.png);">
								<strong>Стоимость <br>1 <span>м<sup>2</sup></span></strong>
								<p>от 145 000 руб.</p>
								<div class="number-item">
									<span>02</span>
								</div>
							</div>
						</div>
					</div>
					<div class="intro__video-content">
						<strong>Смотреть видео <br>о проекте</strong>
						<a data-fancybox href="#myVideo" class="play-btn-wrap  glare-btn">
							<img src="img/play-btn.png" alt="play-btn">
							<div class="glare"></div>
						</a>
					</div>
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/sky-p-1.png" alt="sky-parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.12">
							<img src="img/sky-p-2.png" alt="sky-parallax">
						</div>
					</div>
				</div>
			</section>
		</div>

		<section class="about">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">Современный<br><span>многоквартирный</span><br>11 этажный жилой дом</h2>
					<p class="title-block__text">Архитектура комплекса — это сочетание дизайна и технологий</p>
				</div>	
				<div class="about__list">
					<div class="about__item">
						<div class="about__item-img">
							<img src="img/about-1.jpg" alt="about">
						</div>
						<p>Комплекс строиться<br>по монолитной технологии с использованием качественных материалов</p>
					</div>
					<div class="about__item">
						<div class="about__item-img">
							<img src="img/about-2.jpg" alt="about">
						</div>
						<p>Вентилируемые фасады, панорамное остекление SOLAR с графитовым напылением</p>
					</div>
					<div class="about__item">
						<div class="about__item-img">
							<img src="img/about-3.jpg" alt="about">
						</div>
						<p>Витражные двухкамерные стеклопакеты с алюминиевым профилем фирмы SOLAR</p>
					</div>
					<div class="about__item">
						<div class="about__item-img">
							<img src="img/about-4.jpg" alt="about">
						</div>
						<p>Два бесшумных лифта OTIS</p>
						<span class="about__item-ammount">x2</span>
					</div>
				</div>
			</div>
		</section>

		<section class="resting" style="background-image: url(img/resting-bg.jpg);">
			<div class="container">
				<div class="resting__title-content">
					<div class="title-block">
						<h2 class="title-block__title">ЖК «Александрит» — <span>ощущение комфорта и отдыха</span></h2>
					</div>	
					<p class="resting__description">Двор, который понравится всем</p>
				</div>
				<div class="resting__list">
					<div class="resting__item">
						<div class="resting__item-img">
							<img src="img/resting-1.jpg" alt="resting-item">
						</div>
						<div class="resting__item-text">
							<p><span>Закрытая охраняемая</span> территория, <span>консьерж</span> сервис</p>
						</div>
					</div>
					<div class="resting__item">
						<div class="resting__item-img">
							<img src="img/resting-2.jpg" alt="resting-item">
						</div>
						<div class="resting__item-text">
							<p><span>Детская</span> и <span>спортивная</span> площадка со современным инвентарем</p>
						</div>
					</div>
					<div class="resting__item">
						<div class="resting__item-img">
							<img src="img/resting-3.jpg" alt="resting-item">
						</div>
						<div class="resting__item-text">
							<p><span>Трехуровневый</span><br>паркинг</p>
						</div>
					</div>
					<div class="resting__item">
						<div class="resting__item-img">
							<img src="img/resting-4.jpg" alt="resting-item">
						</div>
						<div class="resting__item-text">
							<p><span>Ландшафтное</span><br>озеленение</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="location">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">ЖК «Александрит» расположен в тихом центре Сочи, где его окружает <span>малоэтажная застройка <br>с зелеными дворами</span></h2>
				</div>	
				<div class="location__main">
					<div class="location__map-wrap">
						<div id="map-2"></div>
						<div id="marker-tooltip"></div>
					</div>
					<div class="location__content">
						<div class="location__content-title">
							<div class="location__content-title-wrap">
								<p>Место застройки полностью обеспечено всей современной городской инфраструктурой</p>
							</div>
						</div>
						<ul class="main-list">
							<li>Более 10 различных направлений общественного транспорта</li>
							<li>В 5-ти минутах ходьбы расположен больничный <br>городок, супермаркеты, детские сады и школы</li>
							<li>До моря 25 минут пешей <br>прогулки</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.015">
					<img src="img/bush.png" alt="bush">
				</div>
				<div class="parallax-item  layer" data-depth="0.02">
					<img src="img/tree-2.png" alt="tree">
				</div>
				<div class="parallax-item  layer" data-depth="0.05">
					<img src="img/bird-p-5.png" alt="bird">
				</div>
				<div class="parallax-item  layer" data-depth="0.2">
					<img src="img/bird-p-6.png" alt="bird">
				</div>
			</div>
		</section>

		<section class="panorama">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">Панорама <span>360°</span></h2>
				</div>	
			</div>
			<div class="panorama__content" style="background-image: url(img/panorama-bg.jpg);">
				<div class="intro__video-content">
					<strong>Смотреть <br>панораму</strong>
					<a data-fancybox data-src="#popup-pano" href="javascript:;" class="play-btn-wrap">
						<img src="img/play-btn.png" alt="play-btn">
					</a>
				</div>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.05">
					<img src="img/sky-p-2.png" alt="sky-parallax">
				</div>
			</div>
		</section>

		<section class="view" style="background-image: url(img/view-bg.jpg);">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">Квартиры с видом <br><span>на море и горы</span></h2>
					<p class="title-block__text">Витражное остекление и открытые балконы</p>
				</div>	
				<div class="view__list">
					<div class="view__item">
						<div class="view__item-img">
							<img src="img/view-1.jpg" alt="view-item">
							<div class="number-item">
								<span>01</span>
							</div>
						</div>
						<div class="view__item-text">
							<p>Такое решение помогает обеспечить нужную <span>инсоляцию квартир</span></p>
						</div>	
					</div>
					<div class="view__item">
						<div class="view__item-img">
							<img src="img/view-2.jpg" alt="view-item">
							<div class="number-item">
								<span>02</span>
							</div>
						</div>
						<div class="view__item-text">
							<p>Квартиры <span>светлые</span> <br>и <span>уютные</span></p>
						</div>	
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.5">
						<img src="img/bird-p-1.png" alt="bird">
					</div>
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/bird-p-2.png" alt="bird">
					</div>
				</div>
			</div>
		</section>

		<section class="variants">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">Варианты <span>квартир</span></h2>
				</div>	
				<div class="variants__list">
					<a data-fancybox="flat-1" href="img/variant-1.jpg" class="variants__item">
						<div class="variants__item-img">
							<img src="img/variant-1.jpg" alt="variant-house">
						</div>
						<div class="variants__item-text">
							<p>Студии</p>
							<span>27 м<sup>2</sup></span>
						</div>
						<p class="variants__price">от 4,7 млн. руб.</p>
					</a>
					<a data-fancybox="flat-2" href="img/variant-2.jpg" class="variants__item">
						<div class="variants__item-img">
							<img src="img/variant-2.jpg" alt="variant-house">
						</div>
						<div class="variants__item-text">
							<p>Однокомнатные</p>
							<p>квартиры <span>33 м<sup>2</sup></span></p>
						</div>
						<p class="variants__price">от 4,8 млн. руб.</p>
					</a>
					<a data-fancybox="flat-3" href="img/variant-3.jpg" class="variants__item">
						<div class="variants__item-img">
							<img src="img/variant-3.jpg" alt="variant-house">
						</div>
						<div class="variants__item-text">
							<p>Двухкомнатные</p>
							<p>квартиры <span>51 м<sup>2</sup></span></p>
						</div>
						<p class="variants__price">от 8,4 млн. руб.</p>
					</a>
					<a data-fancybox="flat-4" href="img/variant-4.jpg" class="variants__item">
						<div class="variants__item-img">
							<img src="img/variant-4.jpg" alt="variant-house">
						</div>
						<div class="variants__item-text">
							<p>Трехкомнатные</p>
							<p>квартиры <span>64.2 м<sup>2</sup></span></p>
						</div>
						<p class="variants__price">от 9,3 млн. руб.</p>
					</a>
				</div>
				<a data-fancybox data-src="#popup-5" href="javascript://" class="btn-main  btn-main--col"><span>Получить актуальные<br>цены и планировки</span></a>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.02">
					<img src="img/tree.png" alt="tree">
				</div>
			</div>
		</section>

		<section class="conditions" style="background-image: url(img/conditions-bg.jpg);">
			<div class="container">
				<div class="conditions__content">
					<div class="title-block">
						<h2 class="title-block__title"><span>Выгодные условия </span>рассрочки</h2>
						<p class="title-block__text">Рассрочка до 4 месяцев<br>Первый взнос от 50%</p>
					</div>
					<div class="conditions__list">
						<div class="conditions__item">
							<div class="conditions__item-img">
								<img src="img/doc.png" alt="document">
							</div>
							<div class="conditions__item-text">
								<p>Предоставляем несколько вариантов рассрочки <br>на выбор</p>
							</div>
						</div>
						<div class="conditions__item">
							<div class="conditions__item-img">
								<img src="img/cond-sec.png" alt="woman">
							</div>
							<div class="conditions__item-text">
								<p>Условия и схема рассрочки разрабатываются индивидуально для каждого клиента</p>
							</div>
						</div>
					</div>
				</div>
				<div class="form-content">
					<div class="form-content__wrap">
						<strong class="form-content__title">Оставьте заявку</strong>
						<p class="form-content__subtitle">и получите индивидуальные<br>условия рассрочки</p>
						<form action="" novalidate="novalidate" class="form-content__form  name-tel-form">
							<div class="form-content__field">
								<input type="text" name="name" placeholder="Введите ваше имя">
							</div>
							<div class="form-content__field">
								<input type="tel" name="tel" placeholder="Введите ваш телефон">
							</div>
							<button class="btn-main  btn-main--col"><span>Получить индивидуальные<br>условия</span></button>
							<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
							<input type="hidden" name="form_name" value="Индивидуальные условия рассрочки">
							<input type="hidden" name="ym_id" value="form_sub_04">
							<input type="hidden" name="ga_id" value="sub_04">
							<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
						</form>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.1">
						<img src="img/bird-p-4.png" alt="bird">
					</div>
					<div class="parallax-item  layer" data-depth="0.7">
						<img src="img/bird-p-3.png" alt="bird">
					</div>
				</div>
			</div>
		</section>

		<section class="deals">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title"><span>Гарантии надежной</span><br>сделки</h2>
				</div>
				<div class="deals__content">
					<div class="deals__list">
						<div class="deals__item">
							<div class="deals__item-image">
								<img src="img/deals-1.jpg" alt="deals-item">
							</div>
							<p>Объект строится в соответствии со всеми федеральными<br>и региональными законами</p>
						</div>
						<div class="deals__item">
							<div class="deals__item-image">
								<img src="img/deals-2.jpg" alt="deals-item">
							</div>
							<p>Все сделки сопровождаются профессиональными юристами, которые готовы предоставить необходимые документы при покупке квартиры</p>
						</div>
					</div>
					<div class="deals__documents">
						<div class="deals__document-item">
							<strong class="deals__documents-file">pdf</strong>
							<a href="#" class="deals__document-link">Разрешение <br>на строительство</a>
						</div>
						<div class="deals__document-item">
							<strong class="deals__documents-file">pdf</strong>
							<a href="#" class="deals__document-link">Свидетельство о праве собственности земельного участка</a>
						</div>
					</div>
				</div>
				<a data-fancybox data-src="#popup-3" href="javascript://" class="btn-main  btn-main--col"><span>Запросить полный<br>пакет документов</span></a>
			</div>
		</section>

		<section class="flight" style="background-image: url(img/flight.jpg);">
			<div class="container">
				<div class="flight__content">
					<div class="title-block">
						<h2 class="title-block__title">Перелет <br><span>за наш счет</span></h2>
						<p class="title-block__text">Для полноценного ознакомления с проектом <br>и выбором квартиры приезжайте на строительную площадку в Сочи</p>
					</div>
					<strong class="flight__list-title">Без лишних затрат Вы сможете</strong>
					<ul class="main-list">
						<li>Увидеть, как ведутся работы на проекте</li>
						<li>Задать все интересующие Вас вопросы</li>
						<li>Познакомиться с инфраструкторой района</li>
					</ul>
				</div>
				<div class="form-content">
					<div class="form-content__wrap">
						<strong class="form-content__title">Введите ваши данные,</strong>
						<p class="form-content__subtitle">чтобы оставить заявку<br>на презентацию объекта в Сочи</p>
						<form action="" novalidate="novalidate" class="form-content__form  name-tel-form">
							<div class="form-content__field">
								<input type="text" name="name" placeholder="Введите ваше имя">
							</div>
							<div class="form-content__field">
								<input type="tel" name="tel" placeholder="Введите ваш телефон">
							</div>
							<button class="btn-main  btn-main--col"><span>хочу приехать<br>на презентацию</span></button>
							<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
							<input type="hidden" name="form_name" value="Заявка на презентацию объекта в Сочи">
							<input type="hidden" name="ym_id" value="form_sub_06">
							<input type="hidden" name="ga_id" value="sub_06">
							<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
						</form>
					</div>
				</div>
			</div>
		</section>

		<section class="contacts">
			<div class="container">
				<div class="title-block">
					<h2 class="title-block__title">Контакты<br><span>отдела продаж</span></h2>
					<p class="title-block__text">Офис продаж находися на объекте ЖК «Александрит»</p>
				</div>
				<div class="contacts__map-wrap">
					<div id="map"></div>
					<div class="contacts__content">
						<div class="contacts__content-wrap">
							<div class="contacts__item">
								<strong>адрес</strong>
								<p>ул. Дагомысская 27</p>
							</div>
							<div class="contacts__item">
								<strong>Номер телефона</strong>
								<a href="tel:+79683002099" class="jsTrackContact">+7 968 300-20-99</a>
							</div>
							<div class="contacts__item">
								<strong>Почта</strong>
								<a href="mailto:info@aleksandritsochi.ru" class="jsTrackContact">info@aleksandritsochi.ru</a>
							</div>
							<div class="contacts__item">
								<strong>Режим работы</strong>
								<p>Ежедневно  с 10.00 до 18.00</p>
								<p>Суббота с 10.00 до 15.00</p>
							</div>
							<a data-fancybox data-src="#popup-4" href="javascript://" class="btn-main  btn-main--col"><span>Заказать обратный<br>звонок</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.03">
					<img src="img/contact-tree.png" alt="tree">
				</div>
				<div class="parallax-item  layer" data-depth="0.2">
					<img src="img/bird-p-5.png" alt="bird">
				</div>
			</div>
		</section>

		<footer class="footer">
			<div class="container">
				<a href="/" class="footer__logo">
					<img src="img/logo-footer.svg" alt="logo-footer">
					<span>Жилой комплекс бизнес класса</span>
				</a>
				<div class="footer__info-content">
					<ul class="footer-links">
						<li><a href="#">Проектные декларации</a></li>
						<li><a href="#">Разрешительная документация</a></li>
						<li><a href="#">Политика конфиденциальности</a></li>
					</ul>
					<div class="footer__social-list">
						<a href="#" target="_blank" class="footer__social vk">
							<svg x="0px" y="0px" width="16"
								 viewBox="0 0 304.36 304.36" style="enable-background:new 0 0 304.36 304.36;" xml:space="preserve" fill="#ffffff">
								<path id="XMLID_807_" style="fill-rule:evenodd;clip-rule:evenodd;" d="M261.945,175.576c10.096,9.857,20.752,19.131,29.807,29.982
									c4,4.822,7.787,9.798,10.684,15.394c4.105,7.955,0.387,16.709-6.746,17.184l-44.34-0.02c-11.436,0.949-20.559-3.655-28.23-11.474
									c-6.139-6.253-11.824-12.908-17.727-19.372c-2.42-2.642-4.953-5.128-7.979-7.093c-6.053-3.929-11.307-2.726-14.766,3.587
									c-3.523,6.421-4.322,13.531-4.668,20.687c-0.475,10.441-3.631,13.186-14.119,13.664c-22.414,1.057-43.686-2.334-63.447-13.641
									c-17.422-9.968-30.932-24.04-42.691-39.971C34.828,153.482,17.295,119.395,1.537,84.353C-2.01,76.458,0.584,72.22,9.295,72.07
									c14.465-0.281,28.928-0.261,43.41-0.02c5.879,0.086,9.771,3.458,12.041,9.012c7.826,19.243,17.402,37.551,29.422,54.521
									c3.201,4.518,6.465,9.036,11.113,12.216c5.142,3.521,9.057,2.354,11.476-3.374c1.535-3.632,2.207-7.544,2.553-11.434
									c1.146-13.383,1.297-26.743-0.713-40.079c-1.234-8.323-5.922-13.711-14.227-15.286c-4.238-0.803-3.607-2.38-1.555-4.799
									c3.564-4.172,6.916-6.769,13.598-6.769h50.111c7.889,1.557,9.641,5.101,10.721,13.039l0.043,55.663
									c-0.086,3.073,1.535,12.192,7.07,14.226c4.43,1.448,7.35-2.096,10.008-4.905c11.998-12.734,20.561-27.783,28.211-43.366
									c3.395-6.852,6.314-13.968,9.143-21.078c2.096-5.276,5.385-7.872,11.328-7.757l48.229,0.043c1.43,0,2.877,0.021,4.262,0.258
									c8.127,1.385,10.354,4.881,7.844,12.817c-3.955,12.451-11.65,22.827-19.174,33.251c-8.043,11.129-16.645,21.877-24.621,33.072
									C252.26,161.544,252.842,166.697,261.945,175.576L261.945,175.576z M261.945,175.576"/>
							</svg>
						</a>
						<a href="#" target="_blank" class="footer__social fb">
							<svg x="0px" y="0px" width="15"
								 viewBox="0 0 310 310" style="enable-background:new 0 0 310 310;" xml:space="preserve" fill="#ffffff">
								<path id="XMLID_835_" d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064
									c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996
									V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545
									C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703
									c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"/>
							</svg>
						</a>
						<a href="#" target="_blank" class="footer__social inst">
							<svg viewBox="0 0 512.00096 512.00096" width="13" xmlns="http://www.w3.org/2000/svg" fill="#ffffff"><path d="m373.40625 0h-234.8125c-76.421875 0-138.59375 62.171875-138.59375 138.59375v234.816406c0 76.417969 62.171875 138.589844 138.59375 138.589844h234.816406c76.417969 0 138.589844-62.171875 138.589844-138.589844v-234.816406c0-76.421875-62.171875-138.59375-138.59375-138.59375zm-117.40625 395.996094c-77.195312 0-139.996094-62.800782-139.996094-139.996094s62.800782-139.996094 139.996094-139.996094 139.996094 62.800782 139.996094 139.996094-62.800782 139.996094-139.996094 139.996094zm143.34375-246.976563c-22.8125 0-41.367188-18.554687-41.367188-41.367187s18.554688-41.371094 41.367188-41.371094 41.371094 18.558594 41.371094 41.371094-18.558594 41.367187-41.371094 41.367187zm0 0"/><path d="m256 146.019531c-60.640625 0-109.980469 49.335938-109.980469 109.980469 0 60.640625 49.339844 109.980469 109.980469 109.980469 60.644531 0 109.980469-49.339844 109.980469-109.980469 0-60.644531-49.335938-109.980469-109.980469-109.980469zm0 0"/><path d="m399.34375 96.300781c-6.257812 0-11.351562 5.09375-11.351562 11.351563 0 6.257812 5.09375 11.351562 11.351562 11.351562 6.261719 0 11.355469-5.089844 11.355469-11.351562 0-6.261719-5.09375-11.351563-11.355469-11.351563zm0 0"/></svg>
						</a>
					</div>
					<div class="footer__contacts">
						<a href="tel:89683002099" class="footer__tel jsTrackContact">8 968 300 20 99</a>
						<a href="mailto:info@aleksandritsochi.ru" class="footer__mail jsTrackContact">info@aleksandritsochi.ru</a>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="popup  form-content" id="popup-1">
		<div class="form-content__wrap">
			<strong class="form-content__title">ВЕДИТЕ ВАШ<br>НОМЕР ТЕЛЕФОНА,</strong>
			<p class="form-content__subtitle">и специалист отдела<br>продаж свяжется с Вами</p>
			<form action="" novalidate="novalidate"  class="form-content__form  tel-form">
				<div class="form-content__field">
					<input type="tel" name="tel" placeholder="Введите ваш телефон">
				</div>
				<button class="btn-main"><span>позвоните мне</span></button>
				<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
				<input type="hidden" name="form_name" value="Перезвонить Клиенту">
				<input type="hidden" name="ym_id" value="form_sub_01">
				<input type="hidden" name="ga_id" value="sub_01">
				<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
			</form>
			<img src="img/popup-b-1.png" alt="bird" class="popup__bird">
			<img src="img/popup-t-1.jpg" alt="tree" class="popup__tree">
		</div>
	</div>

	<div class="popup  form-content" id="popup-4">
		<div class="form-content__wrap">
			<strong class="form-content__title">ВЕДИТЕ ВАШ<br>НОМЕР ТЕЛЕФОНА,</strong>
			<p class="form-content__subtitle">и специалист отдела<br>продаж свяжется с Вами</p>
			<form action="" novalidate="novalidate"  class="form-content__form  tel-form">
				<div class="form-content__field">
					<input type="tel" name="tel" placeholder="Введите ваш телефон">
				</div>
				<button class="btn-main"><span>позвоните мне</span></button>
				<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
				<input type="hidden" name="form_name" value="Перезвонить Клиенту">
				<input type="hidden" name="ym_id" value="form_sub_07">
				<input type="hidden" name="ga_id" value="sub_07">
				<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
			</form>
			<img src="img/popup-b-1.png" alt="bird" class="popup__bird">
			<img src="img/popup-t-1.jpg" alt="tree" class="popup__tree">
		</div>
	</div>

	<div class="popup  popup-big  form-content" id="popup-2">
		<div class="form-content__wrap">
			<div class="popup__bg" style="background-image: url(img/popup-bg-2.jpg);"></div>
			<div class="popup__container">
				<strong class="form-content__title">получить актуальные<br>цены и планировки квартир</strong>
				<p class="form-content__subtitle">Заполните поля ниже<br>и специалист отдела продаж отправит<br>документ по указанному E-mail</p>
				<form action="" novalidate="novalidate" class="form-content__form  name-tel-form">
					<div class="form-content__field">
						<input type="text" name="name" placeholder="Введите ваше имя">
					</div>
					<div class="form-content__field">
						<input type="tel" name="tel" placeholder="Введите ваш телефон">
					</div>
					<button class="btn-main  btn-main--col"><span>Получить актуальные<br>цены и планировки</span></button>
					<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
					<input type="hidden" name="form_name" value="Актуальные цены и планировки квартир">
					<input type="hidden" name="ym_id" value="form_sub_02">
					<input type="hidden" name="ga_id" value="sub_02">
					<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
				</form>
				<img src="img/bird-p-1.png" alt="bird" class="popup__bird-1">
			</div>
		</div>
	</div>
	<div class="popup  popup-big  form-content" id="popup-5">
		<div class="form-content__wrap">
			<div class="popup__bg" style="background-image: url(img/popup-bg-2.jpg);"></div>
			<div class="popup__container">
				<strong class="form-content__title">получить актуальные<br>цены и планировки квартир</strong>
				<p class="form-content__subtitle">Заполните поля ниже<br>и специалист отдела продаж отправит<br>документ по указанному E-mail</p>
				<form action="" novalidate="novalidate" class="form-content__form  name-tel-form">
					<div class="form-content__field">
						<input type="text" name="name" placeholder="Введите ваше имя">
					</div>
					<div class="form-content__field">
						<input type="tel" name="tel" placeholder="Введите ваш телефон">
					</div>
					<button class="btn-main  btn-main--col"><span>Получить актуальные<br>цены и планировки</span></button>
					<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
					<input type="hidden" name="form_name" value="Актуальные цены и планировки квартир">
					<input type="hidden" name="ym_id" value="form_sub_03">
					<input type="hidden" name="ga_id" value="sub_03">
					<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
				</form>
				<img src="img/bird-p-1.png" alt="bird" class="popup__bird-1">
			</div>
		</div>
	</div>

	<div class="popup  form-content" id="popup-3">
		<div class="form-content__wrap">
			<strong class="form-content__title">Получите весь<br>пакет документов<br>для скачивания</strong>
			<p class="form-content__subtitle">Включая договор купли-продажи<br>и проектную декларацию</p>
			<form action="" novalidate="novalidate" class="form-content__form  mail-form">
				<div class="form-content__field">
					<input type="email" name="email" placeholder="Введите ваш E-mail">
				</div>
				<button class="btn-main"><span>скачать пакет документов</span></button>
				<p class="form-content__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
				<input type="hidden" name="form_name" value="Получить весь пакет документов для скачивания">
				<input type="hidden" name="ym_id" value="form_sub_05">
				<input type="hidden" name="ga_id" value="sub_05">
				<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
			</form>
			<img src="img/popup-i-1.jpg" alt="item" class="popup__img-item">
		</div>
	</div>

	<div class="popup  form-content" id="popup-thanks">
		<div class="form-content__wrap">
			<strong class="form-content__title">Спасибо за заявку!</strong>
			<p class="form-content__subtitle">Мы свяжемся с Вами<br>в ближайшее время</p>
		</div>
		<img src="img/thanks-tree.jpg" alt="item" class="popup__img-1">
		<img src="img/bird-p-5.png" alt="bird" class="popup__img-2">
	</div>

	<video width="1280" height="720" controls id="myVideo" style="display:none;">
	    <source src="video.mp4" type="video/mp4">
	    Your browser doesn't support HTML5 video tag.
	</video>

    <div class="popup-pano" id="popup-pano">
        <iframe src="/pano/index.html" frameborder="0" style="display: block;width: 100%; height: 100%;"></iframe>
    </div>


	<script src="js/vendor.js"></script>
	<script src="js/main.js"></script>
	<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuAYwH09nmwECVPXh3_ZP1anJXMO8qyGM&amp;callback=initMap"></script>
</body>
</html>