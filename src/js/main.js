var map,
    mapFooter;
function initMap() {
    var mapCenterCoords;
    if ($(window).width() > 579) {
        mapCenterCoords = {lat: 43.5997899, lng: 39.7387642};
        mapCenterCoords_2 = {lat: 43.598073, lng: 39.731774};
    } else {
        mapCenterCoords = {lat: 43.5994811, lng: 39.7396542};
        mapCenterCoords_2 = {lat: 43.6001111, lng: 39.7394842};
    }

    mapFooter = new google.maps.Map(document.getElementById('map'), {
        center: mapCenterCoords,
        zoom: 18,
        scrollwheel: false,
        zoomControl: false,
        disableDefaultUI: true,
        styles: [

         {elementType: 'labels.text.stroke', stylers: [{color: '#ffffff'}]},
         {elementType: 'labels.text.fill', stylers: [{color: '#84898b'}]},
         {
           featureType: 'administrative.locality',
           elementType: 'labels.text.fill',
           stylers: [{color: '#84898b'}]
         },
         {
           featureType: 'poi',
           elementType: 'labels.text.fill',
           stylers: [{color: '#84898b'}]
         },
         {
           featureType: 'poi.park',
           elementType: 'labels.text.stroke',
           stylers: [{color: '#ffffff'}]
         },
         {
           featureType: 'poi',
           elementType: 'geometry',
           stylers: [{color: '#e5e5e5'}]
         },
         {
           featureType: 'road',
           elementType: 'geometry',
           stylers: [{color: '#ffffff'}]
         },
         {
           featureType: 'road',
           elementType: 'geometry.stroke',
           stylers: [{color: '#e5e5e5'}]
         },
         {
           featureType: 'road',
           elementType: 'labels.text.fill',
           stylers: [{color: '#84898b'}]
         },
         {
           featureType: 'road.highway',
           elementType: 'geometry',
           stylers: [{color: '#d8d8d8'}]
         },
         {
           featureType: 'road.highway',
           elementType: 'geometry.stroke',
           stylers: [{color: '#acacac'}]
         },
         {
           featureType: 'road.highway',
           elementType: 'labels.text.fill',
           stylers: [{color: '#414141'}]
         },
         {
           featureType: 'transit',
           elementType: 'geometry',
           stylers: [{color: '#ffffff'}]
         },
         {
           featureType: 'transit.station',
           elementType: 'labels.text.fill',
           stylers: [{color: '#ffffff'}]
         },
         {
             featureType: "poi.business",
             elementType: "labels",
             stylers: [
               {
                 visibility: "off"
               }
             ]
           }
        ]
    });
    var markerFooter;
    markerFooter = new google.maps.Marker({
        position: {lat: 43.5997899, lng: 39.7396642},
        map: mapFooter,
        icon:"img/map-marker.png"
    });

    map = new google.maps.Map(document.getElementById('map-2'), {
        center: mapCenterCoords_2,
        zoom: 16,
        scrollwheel: false,
        zoomControl: false,
        disableDefaultUI: true,
        styles: [

     {elementType: 'labels.text.stroke', stylers: [{color: '#ffffff'}]},
     {elementType: 'labels.text.fill', stylers: [{color: '#84898b'}]},
     {
       featureType: 'administrative.locality',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi.park',
       elementType: 'labels.text.stroke',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'poi',
       elementType: 'geometry',
       stylers: [{color: '#e5e5e5'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry.stroke',
       stylers: [{color: '#e5e5e5'}]
     },
     {
       featureType: 'road',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry',
       stylers: [{color: '#d8d8d8'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry.stroke',
       stylers: [{color: '#acacac'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'labels.text.fill',
       stylers: [{color: '#414141'}]
     },
     {
       featureType: 'transit',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'transit.station',
       elementType: 'labels.text.fill',
       stylers: [{color: '#ffffff'}]
     },
     {
         featureType: "poi.business",
         elementType: "labels",
         stylers: [
           {
             visibility: "off"
           }
         ]
       }
    ]
    });
    function customZoomBtns(map, btnsPosition) {
        function ZoomControl(controlDiv, map) {
            controlDiv.style.padding = '10px';
            var controlWrapper = document.createElement('div');
            controlWrapper.style.textAlign = 'center';
            controlWrapper.style.width = '40px';
            controlDiv.appendChild(controlWrapper);
            var zoomInButton = document.createElement('div');
            zoomInButton.style.width = '40px';
            zoomInButton.style.height = '40px';
            zoomInButton.style.marginTop = '56px';
            zoomInButton.setAttribute("class", "map-zoom-in");
            controlWrapper.appendChild(zoomInButton);
            var zoomOutButton = document.createElement('div');
            zoomOutButton.style.width = '40px';
            zoomOutButton.style.height = '40px';
            zoomOutButton.style.marginTop = '12px';
            zoomOutButton.setAttribute("class", "map-zoom-out");
            controlWrapper.appendChild(zoomOutButton);
            google.maps.event.addDomListener(zoomInButton, 'click', function() {
                map.setZoom(map.getZoom() + 1);
            });
            google.maps.event.addDomListener(zoomOutButton, 'click', function() {
                map.setZoom(map.getZoom() - 1);
            });
        }


        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);
        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition[btnsPosition]].push(zoomControlDiv);
    }

    customZoomBtns(map, 'TOP_RIGHT');
    customZoomBtns(mapFooter, 'RIGHT_BOTTOM');



    var marker_1 = 'img/marker-1.png',
        marker_2 = 'img/marker-2.png',
        marker_3 = 'img/marker-3.png',
        marker_4 = 'img/marker-4.png',
        marker_5 = 'img/marker-5.png',
        objects = [
            [43.5997899,39.7395642, marker_1, 'ЖК «Александрит»', 'big'],
            [43.598775, 39.739672, marker_2, 'Больница'],
            [43.594903, 39.737268, marker_2, 'Больница'],
            [43.601001, 39.735059, marker_3, 'Детский сад'],
            [43.597951, 39.736249, marker_3, 'Детский сад'],
            [43.596442, 39.733157, marker_3, 'Детский сад'],
            [43.595050, 39.732819, marker_3, 'Детский сад'],
            [43.592915, 39.737564, marker_3, 'Детский сад'],
            [43.591148, 39.736883, marker_3, 'Детский сад'],
            [43.597170, 39.733460, marker_4, 'Школа'],
            [43.591672, 39.736314, marker_4, 'Школа'],
            [43.601539,39.733982, marker_5, 'Магазин'],
            [43.598754, 39.738042, marker_5, 'Магазин'],
            [43.596031, 39.734939, marker_5, 'Магазин'],
            [43.593593, 39.736872, marker_5, 'Магазин'],
            [43.595065, 39.741947, marker_5, 'Магазин'],
        ];

    function fromLatLngToPoint(latLng, map) {
        var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
        var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
        var scale = Math.pow(2, map.getZoom());
        var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
        return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
    }

    function addTooltipObject(marker, text) {
        marker.addListener('mouseover', function () {
            var point = fromLatLngToPoint(marker.getPosition(), map);
            $('#marker-tooltip').html('<strong>' + text + '</strong>').css({
                'left': point.x - 36,
                'top': point.y - 130
            }).show();
        });
        marker.addListener( 'mouseout', function () {
            $('#marker-tooltip').hide();
        });
    }
    function setMarkers(map) {
        for (var j = 0; j < objects.length; j++) {
            var object = objects[j];
            var objMarker = new google.maps.Marker({
                position: {lat: object[0], lng: object[1]},
                map: map,
                icon: {
                    url: object[2],
                    size: (object[4] === 'big') ? (new google.maps.Size(57, 61)) : (new google.maps.Size(43, 43))
                }

            });
            addTooltipObject(objMarker, object[3]);
        }
    }
    setMarkers(map);


};



$(function () {


  $.extend( $.validator.messages, {
    required: "Это поле необходимо заполнить.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный адрес электронной почты.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
  } );




	if ($(window).width() > 1023) {

	  $('.parallax-wrap').parallax({
	    calibrateX: true,
	    calibrateY: false,
	    limitX: false,
	    limitY: false,
	    scalarX: 2,
	    scalarY: 10,
	    frictionX: 0.2,
	    frictionY: 0.2
	  });

	}


    function YMGoal(goal_id) {
	    yaCounter52574287.reachGoal(goal_id);
	    return true;
    }
    function GASendForm(id) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'form',
            eventAction: id
        });
    }


  $('.tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 mobileRU: true,
                 required: true
             },

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 /* берем из формы id цели яндекс метрики */
                 var $ym_input = $form.find('input[name="ym_id"]');
                 if ($ym_input.length > 0) {
                     var ym_id = $ym_input.val();
                     /* отправляем цель в я.метрику */
                     YMGoal(ym_id);
                 }
                 /* берем из формы id события GA */
                 var $ga_input = $form.find('input[name="ga_id"]');
                 if ($ga_input.length > 0) {
                     var ga_id = $ga_input.val();
                     /* отправляем в GA */
                     GASendForm(ga_id);
                 }

                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 $.fancybox.close();
                 $.fancybox.open({
                     src:'#popup-thanks'
                 });

                 fbq('track', 'Lead_aleksandrit', {
                     value: 5,
                 });
                 VK.Retargeting.Add(29348523);
                 VK.Retargeting.Event('Lead');

                 /* Rating@Mail.ru counter */
                 _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Lead' });
                 /* //Rating@Mail.ru counter */
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.name-tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 mobileRU: true,
                 required: true
             },
             name: {
                 required: true
             }

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             name: {
                 required: 'Укажите ваше имя'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "name-tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 /* берем из формы id цели яндекс метрики */
                 var $ym_input = $form.find('input[name="ym_id"]');
                 if ($ym_input.length > 0) {
                     var ym_id = $ym_input.val();
                     /* отправляем цель в я.метрику */
                     YMGoal(ym_id);
                 }
                 /* берем из формы id события GA */
                 var $ga_input = $form.find('input[name="ga_id"]');
                 if ($ga_input.length > 0) {
                     var ga_id = $ga_input.val();
                     /* отправляем в GA */
                     GASendForm(ga_id);
                 }

                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 $.fancybox.close();
                 $.fancybox.open({
                     src:'#popup-thanks'
                 });

                 fbq('track', 'Lead_aleksandrit', {
                     value: 5,
                 });
                 VK.Retargeting.Add(29348523);
                 VK.Retargeting.Event('Lead');

                 /* Rating@Mail.ru counter */
                 _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Lead' });
                 /* //Rating@Mail.ru counter */
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.tel-mail-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 mobileRU: true,
                 required: true
             },
             email: {
                 required: true
             }

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             email: {
                 required: 'Укажите ваш e-mail'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "tel-mail-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 /* берем из формы id цели яндекс метрики */
                 var $ym_input = $form.find('input[name="ym_id"]');
                 if ($ym_input.length > 0) {
                     var ym_id = $ym_input.val();
                     /* отправляем цель в я.метрику */
                     YMGoal(ym_id);
                 }
                 /* берем из формы id события GA */
                 var $ga_input = $form.find('input[name="ga_id"]');
                 if ($ga_input.length > 0) {
                     var ga_id = $ga_input.val();
                     /* отправляем в GA */
                     GASendForm(ga_id);
                 }

                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 $.fancybox.close();
                 $.fancybox.open({
                     src:'#popup-thanks'
                 });

                 fbq('track', 'Lead_aleksandrit', {
                     value: 5,
                 });
                 VK.Retargeting.Add(29348523);
                 VK.Retargeting.Event('Lead');

                 /* Rating@Mail.ru counter */
                 _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Lead' });
                 /* //Rating@Mail.ru counter */
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.mail-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             email: {
                 required: true
             }

         },
         messages: {
             email: {
                 required: 'Укажите ваш e-mail'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "mail-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 /* берем из формы id цели яндекс метрики */
                 var $ym_input = $form.find('input[name="ym_id"]');
                 if ($ym_input.length > 0) {
                     var ym_id = $ym_input.val();
                     /* отправляем цель в я.метрику */
                     YMGoal(ym_id);
                 }
                 /* берем из формы id события GA */
                 var $ga_input = $form.find('input[name="ga_id"]');
                 if ($ga_input.length > 0) {
                     var ga_id = $ga_input.val();
                     /* отправляем в GA */
                     GASendForm(ga_id);
                 }

                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 $.fancybox.close();
                 $.fancybox.open({
                     src:'#popup-thanks'
                 });

                 fbq('track', 'Lead_aleksandrit', {
                     value: 5,
                 });
                 VK.Retargeting.Add(29348523);
                 VK.Retargeting.Event('Lead');

                 /* Rating@Mail.ru counter */
                 _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Lead' });
                 /* //Rating@Mail.ru counter */
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });


  $('input[name="tel"]').inputmask({mask: "+7 (999) 9999999"});


    $("[data-fancybox]").fancybox({
        afterShow: function( instance, slide ) {
            if (slide.src === "#popup-pano") {
                fbq('track', 'Pano_aleksandrit', {
                    value: 2,
                });
                VK.Retargeting.Event('Pano_aleksandrit');

                /* Rating@Mail.ru counter */
                _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Pano' });
                /* //Rating@Mail.ru counter */
            } else {
                if (slide.contentType === 'html') {
                    fbq('track', 'FormOpen_aleksandrit', {
                        value: 2
                    });
                    VK.Retargeting.Event('FormOpen_aleksandrit');

                    /* Rating@Mail.ru counter */
                    _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'FormOpen' });
                    /* //Rating@Mail.ru counter */
                }
                if (slide.contentType === 'video') {
                    fbq('track', 'Video_aleksandrit', {
                        value: 2
                    });
                    VK.Retargeting.Event('Video_aleksandrit');

                    /* Rating@Mail.ru counter */
                    _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Video' });
                    /* //Rating@Mail.ru counter */
                }
            }
        }
    });
    $('.jsTrackContact').on('click', function () {
        fbq('track', 'Contact_aleksandrit', {
            value: 4
        });
        VK.Retargeting.Event('Contact_aleksandrit');

        /* Rating@Mail.ru counter */
        _tmr.push({ id: '3090707', type: 'reachGoal', goal: 'Contact' });
        /* //Rating@Mail.ru counter */
    });

});